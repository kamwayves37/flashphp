<?php

namespace fshunit\Attest;

use fshunit\Attest\AttestValue;

class Attest
{
    /**
     * @param mixed $actual
     * @return AttestBe
     */
    public function attests($actual)
    {
        return new AttestBe(new AttestValue($actual));
    }

    /**
     * @param mixed $actual 
     * @return AttestThat
     */
    public function attestThat($actual)
    {
        return new AttestThat(new AttestValue($actual));
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     */
    public function attestEquals($expected, $actual)
    {
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     */
    public function attestNotEquals($expected, $actual)
    {
    }

    /**
     * @param mixed $actual
     */
    public function attestIsNull($actual)
    {
    }

    /**
     * @param mixed $actual
     */
    public function attestIsNotNull($actual)
    {
    }

    /**
     * @param mixed $actual
     */
    public function attestTrue($actual)
    {
    }

    /**
     * @param mixed $actual
     */
    public function attestFalse($actual)
    {
    }
}
