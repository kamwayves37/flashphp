<?php

namespace fshunit\Attest;

class AttestBe
{
    /**
     * @var AttestValue $actual
     */
    private $actual;

    /**
     * @param AttestValue $actual
     */
    public function __construct(AttestValue $actual)
    {
        $this->actual = $actual;
    }

    /**
     * @param string $type
     */
    public function type(string $type)
    {
    }

    /**
     * @param mixed $expected
     */
    public function equalTo($expected)
    {
    }

    /**
     * @param mixed $expected
     */
    public function notEqualTo($expected)
    {
    }

    /**
     * 
     */
    public function null()
    {
    }

    /**
     * 
     */
    public function notNull()
    {
    }
    /**
     * 
     */
    public function real()
    {
    }

    /**
     * 
     */
    public function fake()
    {
    }
}
