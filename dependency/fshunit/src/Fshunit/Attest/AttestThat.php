<?php

namespace fshunit\Attest;

use fshunit\Attest\AttestValue;

class AttestThat
{
    /**
     * @var AttestValue $actual
     */
    private $actual;

    /**
     * @param AttestValue $actual
     */
    public function __construct(AttestValue $actual)
    {
        $this->actual = $actual;
    }

    public function is()
    {
        # code...
    }
}
