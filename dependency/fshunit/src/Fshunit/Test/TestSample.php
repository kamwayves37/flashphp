<?php

namespace Fshunit\Test;

use Fshunit\Attest\Attest;
use fshunit\Attest\AttestLogicalOperator;

class TestSample extends Attest
{
    public function setUp()
    {
    }

    public function setDown()
    {
    }

    /**
     * @param mixed $expected
     */
    public function equalTo($expected)
    {
    }

    /**
     * @param string $type
     * @return AttestLogicalOperator
     */
    public function type(string $type)
    {
        return new AttestLogicalOperator();
    }

    /**
     * @return AttestLogicalOperator
     */
    public function not()
    {
        return new AttestLogicalOperator();
    }

    /**
     * @return AttestLogicalOperator
     */
    public function notNull()
    {
        return new AttestLogicalOperator();
    }

    /**
     * 
     */
    public function null()
    {
    }

    /**
     * 
     */
    public function true()
    {
    }

    /**
     * 
     */
    public function false()
    {
    }
}
