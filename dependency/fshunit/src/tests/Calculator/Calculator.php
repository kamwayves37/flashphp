<?php

namespace Tests\Calculator;

class Calculator
{
    /**
     * @param int|double|float $a
     * @param int|double|float $b
     * @return int|double|float
     */
    public function add($a, $b)
    {
        return $a + $b;
    }

    public function mult($a, $b)
    {
        return $a * $b;
    }
}
