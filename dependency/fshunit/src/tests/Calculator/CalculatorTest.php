<?php

namespace Tests\Calculator;

use Fshunit\Test\TestSample;

class ExampleTest extends TestSample
{
    /**@var Calculator $calc */
    private $calc;

    public function setUp()
    {
        $this->calc = new Calculator;
    }

    public function testAdd()
    {
        $this->attestThat(1)->is($this->equalTo(12));

        $this->attestEquals(12, 23);

        $this->attests(23)->equalTo(9);
    }

    public function testMult()
    {
        $this->attestThat(1)->is($this->not($this->equalTo(12)));

        $this->attestNotEquals(12, 23);

        $this->attests(23)->notEqualTo(9);
    }

    public function test()
    {
        $this->attestThat(1)->is($this->null());
        $this->attestThat(1)->is($this->type("integer")->and($this->equalTo(12)));
        $this->attestThat(1)->is($this->not($this->null())->and($this->equalTo(12)));
        $this->attestThat(1)->is($this->notNull()->and($this->equalTo(12)));

        $this->attestIsNull(12);
        $this->attestIsNotNull(12);

        $this->attests(23)->null();

        $this->attestThat(1)->is($this->true());
        $this->attestThat(1)->is($this->false());

        $this->attestTrue(12);
        $this->attestFalse(12);

        $this->attests(23)->real();
        $this->attests(23)->fake();
    }
}
