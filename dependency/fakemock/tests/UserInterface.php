<?php

namespace fakemock\tests;

interface UserInterface
{
    public function getPassword();

    public function getUsername();

    public function setUsername($name = 123);

    public function login(string $username, $password);
}
