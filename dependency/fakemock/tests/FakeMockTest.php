<?php

namespace fakemock\tests;

use fakemock\app\Arguments\Args;
use fakemock\app\FakeMock;
use fakemock\app\Inspector\Expects;

require './autoloader/Autoloader.php';

class FakeMockTest extends FakeMock
{
    public function test()
    {
        FakeMock::generate(\fakemock\tests\UserInterface::class, "MyMock", "UserMocking");

        $mock = FakeMock::create(\fakemock\tests\UserInterface::class);

        $mock
            ->method('login')
            ->arguments(Args::types("string"))
            ->Inspect(Expects::call($this->once()))
            ->willReturn(function ($args) {
                return $args;
            });

        print_r($mock->login("Kams", "123"));

        $mock->investigateBehaviour();
    }
}

$test = new FakeMockTest;

$test->test();
