<?php

namespace fakemock\util;

class Scanner
{
    /**
     * @var \ReflectionClass $reflection
     */
    private $reflection;

    public function __construct(\ReflectionClass $reflection)
    {
        $this->reflection = $reflection;
    }

    public function isAbstract(\ReflectionMethod $method): bool
    {
        return $method->isAbstract();
    }

    /**
     *
     */
    public function getMethods(): string
    {
        $methods = "";

        foreach ($this->reflection->getMethods() as $method) {

            $data = [];

            $signature_parameters = [];

            if ($method->isUserDefined() && $method->isAbstract()) {

                $data['accessibility'] = 'public';

                $data['name'] = $method->getName();

                foreach ($method->getParameters() as $param) {

                    $name = ($param->isPassedByReference() ? " &\${$param->getName()}" : " \${$param->getName()}");

                    $type = ($param->hasType() && $param->allowsNull()) ? "?{$param->getType()}" : $param->getType();

                    $signature = trim($type . $name);

                    if ($param->isOptional()) {
                        $signature .= " = " . (is_string($param->getDefaultValue()) ?
                            $this->stringFormater($param->getDefaultValue()) :
                            $param->getDefaultValue());
                    }

                    $signature_parameters[] = $signature;
                }

                $data['return_type'] = $method->hasReturnType() ? ": " . $method->getReturnType() : '';

                if ($method->getReturnType() != "void") {
                    $data['body'] = "return \$this->invokeMethod(__FUNCTION__, func_get_args(), {$this->stringFormater($method->getReturnType())});";
                } else {
                    $data['body'] = "\$this->invokeMethod(__FUNCTION__, func_get_args(), {$this->stringFormater($method->getReturnType())});";
                }

                $data['signature'] = implode(', ', $signature_parameters);

                $methods .= \fakemock\app\Generator\Generator::generateMethod($data) . PHP_EOL;
            }
        }

        return $methods;
    }

    private function stringFormater($value): string
    {
        return "\"$value\"";
    }

    /**
     *
     */
    public function countMethodes(): int
    {
        return count($this->reflection->getMethods());
    }

    public function getName(): string
    {
        return $this->reflection->getName();
    }

    public function getNamespace(): string
    {
        return $this->reflection->getNamespaceName();
    }

    public function getShortName(): string
    {
        return $this->reflection->getShortName();
    }
}
