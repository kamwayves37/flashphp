<?php

namespace fakemock\app\Types\Stub;

use fakemock\app\Types\Stub\StubException;

interface Stub
{
    /**
     * @param $value La valeur qui sera défini comme valeur stub
     * @return \fakemock\app\Types\Stub\StubValue La valeur stub
     */
    public static function returnValue($value): \fakemock\app\Types\Stub\StubValue;

    /**
     * @param \Exception $exception Lance une exception
     * @return \StubException La valeur stub
     */
    public static function triggerException(\Exception $exception): StubException;

    /**
     * Retourne une valeur par défaut selon le type de retoun de la function
     */
    public static function onDefaultValue();

    /**
     * Retourne une valeur de façon consécutif sur le tableau de valeurs
     *
     * @return any Une valeur de l'iteration
     */
    public static function onEachCalls(...$values);

    /**
     *
     */
    public static function onCallable(\Closure $closure);
}
