<?php

namespace fakemock\app\Types\Stub;

class StubValue extends \fakemock\app\Types\StubType
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * retourne une valeur stub
     * @return $value
     */
    public function getValue()
    {
        return $this->value;
    }
}
