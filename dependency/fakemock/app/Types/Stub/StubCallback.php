<?php

namespace fakemock\app\Types\Stub;

class StubCallBack extends \fakemock\app\Types\StubType
{
    /**
     * @var \Closure $closure
     */
    private $closure;

    public function __construct(\Closure $closure)
    {
        $this->closure = $closure;
    }

    /**
     * retourne une valeur stub
     * @return $value
     */
    public function getValue()
    {
        return ($this->closure->bindTo($this->context))($this->args);
    }
}
