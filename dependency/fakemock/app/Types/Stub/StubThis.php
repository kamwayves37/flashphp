<?php

namespace fakemock\app\Types\Stub;

class StubThis extends \fakemock\app\Types\StubType
{
    /**
     * Retourne l'objet mock parent
     * 
     * @return \fakemock\app\Doubler\Doubler
     */
    public function getValue()
    {
        return $this->context;
    }
}
