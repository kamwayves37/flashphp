<?php

namespace fakemock\app\Types\Stub;

class StubArryIterator extends \fakemock\app\Types\StubType
{
    /**
     * @var any $value
     */
    private $current_value;

    /**
     * @var \Iterator $iterator
     */
    private $iterator;

    public function __construct(array $values)
    {
        $this->iterator = (new \ArrayObject($values))->getIterator();

        $this->current_value = $this->iterator->current();
    }

    public function getValueType()
    {
        return gettype($this->current_value);
    }

    /**
     * Todo: Completer les different type
     */
    public function getValue()
    {
        if ($this->iterator->valid()) {
            $this->current_value = $this->iterator->current();
            $this->iterator->next();
        }

        return $this->current_value;
    }
}
