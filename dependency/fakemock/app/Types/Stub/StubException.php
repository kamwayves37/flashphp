<?php

namespace fakemock\app\Types\Stub;

class StubException extends \fakemock\app\Types\StubType
{
    /**
     * @var \Exception $value
     */
    private $value;

    public function __construct(\Exception $value)
    {
        $this->value = $value;
    }

    /**
     * Leve une exception de type stub $value
     * @throws \Exception $value
     */
    public function getValue()
    {
        throw $this->value;
    }
}
