<?php

namespace fakemock\app\Types\Stub;

class StubDefaultValue extends \fakemock\app\Types\StubType
{
    public function __construct($return_type = null)
    {
        $this->return_type = $return_type;
    }

    /**
     * Todo: Completer les different type
     */
    public function getValue()
    {
        if ($this->return_type === "integer" || $this->return_type === "double") {
            return 0;
        } elseif ($this->return_type === "string") {
            return "";
        } else {
            return;
        }
    }
}
