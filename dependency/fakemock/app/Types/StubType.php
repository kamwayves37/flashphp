<?php

namespace fakemock\app\Types;

/**
 * Determine le type de valeur stub
 */
abstract class StubType implements Type
{
    /**
     * @var \fakemock\app\Doubler\Doubler $context
     */
    protected $context;

    /**
     * Type de retour de la function qui manipule la Stub
     * @var string $return_type
     */
    protected $return_type;

    /**
     * Les arguments passé a la function qui manipule la Stub
     * @var array $args
     */
    protected $args;

    /**
     * retourne le type de la valeur stub
     */
    public function getValueType()
    {
        try {
            return gettype($this->getValue());
        } catch (\Throwable $th) {
            //throw $th;
        }

        return "NULL";
    }

    /**
     * Définir les arguments de la function qui l'utilise
     */
    final public function setArgument(array $args)
    {
        $this->args = $args;
    }

    /**
     * Définir le type de retour de la function qui l'utilise
     */
    final public function setReturnType(string $return_type)
    {
        $this->return_type = $return_type;
    }

    /**
     * Définir le context dans le quel le stub est utilisé
     *
     * @param object  $context
     */
    final public function setMockContext(object $context)
    {
        $this->context = $context;
    }
}
