<?php

namespace fakemock\app\Types;

interface Type
{
    /**
     * retourne une valeur stub
     */
    public function getValue();

    /**
     * retourne le type de la valeur stub
     */
    public function getValueType();

    /**
     * Définir les arguments de la function qui l'utilise
     */
    public function setArgument(array $args);

    /**
     * Définir le type de retour de la function qui l'utilise
     */
    public function setReturnType(string $return_type);

    /**
     * Définir le context dans le quel le stub est utilisé
     *
     * @param object $context
     */
    public function setMockContext(object $context);
}
