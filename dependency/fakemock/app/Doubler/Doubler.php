<?php

namespace fakemock\app\Doubler;

use Exception;
use fakemock\app\Fake\FakeMethod;

trait Doubler
{
    /**
     * @param array $controller_methods_registers
     */
    private $controller_methods_registers = [];

    /**
     * @param array $fake_method_registers
     */
    private $fake_method_registers = [];

    /**
     * Configurer le bouchon.
     *
     * @param string $method_name le nom de la methode a configurer;
     * @return \fakemock\app\Fake\FakeMethod La methode a configurer
     */
    public function method(string $method_name): FakeMethod
    {
        if (method_exists($this, $method_name) || key_exists($method_name, $this->fake_method_registers)) {
            if (!key_exists($method_name, $this->controller_methods_registers)) {
                $cork = new FakeMethod($this, $method_name);
                $this->controller_methods_registers[$method_name] = $cork;
                return $cork;
            }
            return $this->controller_methods_registers[$method_name];
        } else {
            throw new Exception("Le bouchon est introuvable");
        }
    }

    /**
     *
     */
    public function fakeMethod(string $method_name)
    {
        if (!key_exists($method_name, $this->fake_method_registers)) {
            $this->fake_method_registers[$method_name] = \Closure::bind(function ($method_name, $args) {
                $result = $this->invokeMethod($method_name, $args, "");
                unset($this->controller_methods_registers[$method_name]);
                return $result;
            }, $this);
        }

        return $this->method($method_name);
    }

    /**
     *
     */
    public function __call($method_name, $args)
    {
        if (key_exists($method_name, $this->fake_method_registers)) {
            $fake = $this->fake_method_registers[$method_name];
            unset($this->fake_method_registers[$method_name]);
            return $fake($method_name, $args);
        } else {
            throw new \Exception("The method '{$method_name}' does not exist or fake method has already been invoked.");
        }
    }

    /**
     *Permet d'invoquer  un bouchon et retourner sa valeur stub
     *@param string $method_name nom de la methode a invoquer
     *@param string $retun_type type de retour attendu
     */
    private function invokeMethod(string $method_name, array $args, $retun_type)
    {
        if (key_exists($method_name, $this->controller_methods_registers)) {
            return $this->controller_methods_registers[$method_name]->invoke($args, $retun_type);
        } else {
            throw new \Exception("Is a Dummy method {$method_name} is not configure");
        }
    }

    /**
     *
     */
    public function investigateBehaviour()
    {
        foreach ($this->controller_methods_registers as $method) {
            $method->investigate();
        }
    }
}
