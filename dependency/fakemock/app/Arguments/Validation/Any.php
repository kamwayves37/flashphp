<?php

namespace fakemock\app\Arguments\Validation;

use fakemock\app\Arguments\Arguments;

final class Any extends Arguments
{
    /**
     * Permet de valider les arguments d'une fonction
     *
     * @param array $current_args l'argument a examiner
     */
    public function check(array $current_args): bool
    {
        return true;
    }
}
