<?php

namespace fakemock\app\Arguments\Validation;

use fakemock\app\Arguments\Arguments;

final class Type extends Arguments
{
    /**
     * @var bool $all_types
     */
    private $all_types;

    public function __construct(array $types, bool $all_types = false)
    {
        foreach ($types as $value) {
            if (!self::isValidType($value)) {
                throw new \TypeError("the defined type '$value' is not a valid type.");
            }
        }

        parent::__construct($types);

        $this->all_types = $all_types;
    }

    /**
     *
     */
    public static function isValidType(string $type): bool
    {
        switch ($type) {
            case 'NULL':
            case 'array':
            case 'object':
            case 'resource':
            case 'boolean':
            case 'integer':
            case 'float':
            case 'double':
            case 'string':
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Permet de valider les arguments d'une fonction
     *
     * @param array $current_args l'argument a examiner
     */
    public function check(array $current_args)
    {
        $msg = $this->default_message;

        if (!$this->all_types) {
            if (count($this->values) <= count($current_args)) {
                for ($i = 0; $i < count($this->values); $i++) {
                    $this->_check($current_args[$i], $this->values[$i]);
                }
                return true;
            } else {
                throw new \TypeError("The number of parameters passed does not correspond to the number of parameters expected.");
            }
        } else {
            for ($i = 0; $i < count($current_args); $i++) {
                $this->_check($current_args[$i], $this->values[0]);
            }
            return true;
        }
    }

    private function _check($current_arg, $actual_arg)
    {
        $current_type = gettype($current_arg);
        if ($actual_arg !== $current_type) {
            $this->fail("Arguments type validation error", "", $actual_arg, $current_type);
        }
    }
}
