<?php

namespace fakemock\app\Arguments\Validation;

class None extends \fakemock\app\Arguments\Arguments
{
    /**
     * Permet de valider les arguments d'une fonction
     *
     * @param array $current_args l'argument a examiner
     */
    public function check(array $current_args)
    {
        if (!empty($current_args)) {
            $this->fail("Empty arguments validation error", "", " ", implode(", ", $current_args));
        }
        return true;
    }
}
