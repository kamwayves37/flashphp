<?php

namespace fakemock\app\Arguments\Validation;

use ArgumentCountError;
use fakemock\app\Arguments\Arguments;

final class Exactly extends Arguments
{
    /**
     * Permet de valider les arguments d'une fonction
     *
     * @param array $current_args l'argument a examiner
     */
    public function check(array $current_args): bool
    {
        if (count($this->values) <= count($current_args)) {
            for ($i = 0; $i < count($this->values); $i++) {
                if ($this->values[$i] !== $current_args[$i]) {
                    $this->fail("Exact arguments validation error", "", implode(", ", $this->values), implode(", ", $current_args));
                }
            }
            return true;
        } else {
            throw new ArgumentCountError("The number of parameters passed does not correspond to the number of parameters expected.");
        }
    }
}
