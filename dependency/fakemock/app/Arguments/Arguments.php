<?php

namespace fakemock\app\Arguments;

abstract class Arguments
{
    /**
     *
     */
    protected $values;

    /**
     * 
     */
    protected $default_message;

    /**
     *
     */
    public function __construct(array $values = null)
    {
        if ($values !== null) {
            $this->values = $values;
        }

        $this->default_message =  "The past arguments do not match the arguments expected by the controller method.";
    }

    /**
     * Permet de valider les arguments d'une fonction
     *
     * @param array $current_args l'argument a examiner
     */
    abstract public function check(array $current_args);

    /**
     * Permet de déclancher une erreur lorsque les arguments ne sont valident
     * 
     * @param string $error_message
     * @throws \Exception
     */
    final protected function fail(
        string $error_title = "",
        string $error_message = "",
        string $expected = "",
        string $current = ""
    ) {
        $msg = !empty($error_title) ? "[" . $error_title . "] " : "";

        $msg .= !empty($error_message) ? $error_message . PHP_EOL : $this->default_message . PHP_EOL;

        $msg .= !empty($expected) ? "Expected: " . $expected . PHP_EOL : "";
        $msg .= !empty($expected) ? "Current: " . $current . PHP_EOL : "";

        throw new \InvalidArgumentException($msg);
    }
}
