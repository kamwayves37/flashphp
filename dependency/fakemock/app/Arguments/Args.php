<?php

namespace fakemock\app\Arguments;

use fakemock\app\Arguments\Validation\None;
use fakemock\app\Container\MockContainer;

final class Args
{
    /**
     * Précise que la fonction n'accepte aucun paramètre.
     * @return \fakemock\app\Arguments\Validation\None
     */
    public static function none()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Arguments\Validation\None::class);
    }

    /**
     *
     */
    public static function exact(...$values)
    {
        return new \fakemock\app\Arguments\Validation\Exactly($values);
    }

    /**
     *
     */
    public static function any()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Arguments\Validation\Any::class);
    }

    /**
     *
     */
    public static function types(string ...$types)
    {
        return new \fakemock\app\Arguments\Validation\Type($types);
    }

    /**
     *
     */
    public static function allTypes(string $types)
    {
        return new \fakemock\app\Arguments\Validation\Type([$types], true);
    }
}
