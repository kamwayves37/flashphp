<?php

namespace fakemock\app\Generator;

use fakemock\app\Container\MockContainer;

/**
 *
 */
class Generator
{

    /**
     * @param array $data
     * @return string
     */
    public static function generateMethod(array $data): string
    {
        return MockContainer::getContainer()
            ->get("/fakemock/app/Generator/templates/method")
            ->map($data);
    }

    /**
     * @param array $data
     * @return string
     */
    public static function generateClass(array $data): string
    {
        return MockContainer::getContainer()
            ->get("/fakemock/app/Generator/templates/class")
            ->map($data);
    }
}
