namespace %namespace%;

class %name% implements %interface_namespace%
    {
        use %magic_traits%;
        
        %methods%
    }