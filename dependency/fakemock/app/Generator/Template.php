<?php

namespace fakemock\app\Generator;

class Template
{
    /**
     * @var string $file_path
     */
    private $file_path;

    private $marker;
    /**
     * @var string $pattern_struc
     */
    private $pattern_struc;

    public function __construct(string $file_path, string $marker = "%", string $struc_regex = "/\w+/")
    {
        $path_parts = pathinfo($file_path);

        if (file_exists($file_path)) {
            if ($path_parts['extension'] === "tpl") {
                $this->file_path = $file_path;
            } else {
                throw new \Exception("File type error: The file extension {$path_parts['basename']} is incorrect.");
            }
        } else {
            throw new \Exception("No {$path_parts['basename']} file found in the {$path_parts['dirname']} directory");
        }

        /**initialisation du marker */
        if (!empty($marker)) {
            $this->marker = $marker;
        } else {
            throw new \Exception("You must set a marker or use the default marker.");
        }

        /**Initialisation du pattern */
        if (!empty($struc_regex)) {
            if ($this->isValidRegex($struc_regex)) {
                $struc_regex = trim($struc_regex, "/");
                $this->pattern_struc = "/{$this->marker}{$struc_regex}{$this->marker}/";
                if (!$this->isValidRegex($this->pattern_struc)) {
                    throw new \Exception("The pattern expression{$this->pattern_struc} is invalid.");
                }
            } else {
                throw new \Exception("The regular expression {$struc_regex} is invalid.");
            }
        } else {
            throw new \Exception("You must define a regular expression that defines the mapping structure or use the default expression.");
        }
    }

    private function isValidRegex($regex): bool
    {
        return @preg_match("$regex", "") !== false;
    }

    /**
     * Permer de mapper les données fournir dans le template
     *
     * @param array $data Données a mappé
     * @return string Le resultat du mappage avec le template
     */
    public function map(array $data)
    {
        $subject = file_get_contents($this->file_path);

        $match_count = preg_match_all($this->pattern_struc, $subject, $match);

        if ($subject) {

            /**Vérifie s'il y'a des valeurs a matcher */
            if ($match_count > 0) {
                $match_error = 0;
                $match_success = 0;

                foreach ($data as $key => $value) {
                    $regex = "/%{$key}%/i";

                    if (preg_match($regex, $subject, $match)) {
                        $match_success++;
                        $subject = preg_replace($regex, $value, $subject);
                    } else {
                        $match_error++;
                    }
                }

                /**Check valid mapping */
                if ($match_success !== $match_count) {
                    throw new \Exception("The data to be mapped could not be fully mapped into the template.");
                }

                if ($match_error > 0) {
                    throw new \Exception("The data to be mapped contains data that cannot be mapped in the template.");
                }

                return $subject;
            } else {
                throw new \Exception("No value to match for the pattern {$this->pattern_struc} in the template file {$this->file_path} ");
            }
        } else {
            throw new \Exception("The contents of the file could not be recovered.");
        }
    }
}
