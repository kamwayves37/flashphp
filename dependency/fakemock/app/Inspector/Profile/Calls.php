<?php

namespace fakemock\app\Inspector\Profile;

use fakemock\app\Inspector\Event\CallEvent;
use fakemock\app\Inspector\Inspector;

final class Calls implements Inspector
{
    /**
     * @param CallEvent $event;
     */
    private $event;

    public function __construct(CallEvent $event)
    {
        $this->event = $event;
    }

    /**
     *
     */
    public function inspect(\fakemock\app\Fake\FakeMethod $method_controller): bool
    {
        return $this->event->verify($method_controller->getNumberOfCalls()) && $method_controller->wasCalled();
    }

    /**
     * Déclenche une erreur lorsque l'inspection a echoué
     * @throws \Exception
     */
    public function fail()
    {
        throw new \Exception("The class inspection failed because the method was called more than expected.");
    }
}
