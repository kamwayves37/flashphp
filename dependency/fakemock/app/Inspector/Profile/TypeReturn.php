<?php

namespace fakemock\app\Inspector\Profile;

use fakemock\app\Arguments\Validation\Type;
use fakemock\app\Inspector\Inspector;

final class TypeReturn implements Inspector
{
    /**
     * @param string $type;
     */
    private $type;

    public function __construct(string $type)
    {
        if (!\fakemock\app\Arguments\Validation\Type::isValidType($type)) {
            throw new \TypeError("The defined type '$type' is not a valid type.");
        }

        $this->type = $type;
    }

    /**
     * Permet de faire une inpection sur le type de retour d'une function
     */
    public function inspect(\fakemock\app\Fake\FakeMethod $method_controller): bool
    {
        return strtolower($this->type) === strtolower($method_controller->getStubReturnType());
    }

    /**
     * Déclenche une erreur lorsque l'inspection a echoué
     * @throws \Exception
     */
    public function fail()
    {
        throw new \Exception("The class inspection failed because, the type of return expected does not correspond to the type received.");
    }
}
