<?php

namespace fakemock\app\Inspector;

use fakemock\app\Inspector\Event\CallEvent;
use fakemock\app\Inspector\Profile\Calls;
use fakemock\app\Inspector\Profile\TypeReturn;

final class Expects
{
    /**
     * @param CallEvent $event
     * @return Calls
     */
    public static function call(CallEvent $event): Calls
    {
        return new Calls($event);
    }

    /**
     * Spécifie le type de retour attendu l'ors de l'appelle de la fonction.
     * 
     * @param string $type
     * @return TypeReturn
     */
    public static function returnType(string $type): TypeReturn
    {
        return new TypeReturn($type);
    }

    public static function timeout(int $duration)
    {
    }

    public static function customize()
    {
    }
}
