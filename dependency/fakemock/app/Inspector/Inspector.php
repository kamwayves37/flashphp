<?php

namespace fakemock\app\Inspector;

interface Inspector
{
    /**
     * Déclenche une erreur lorsque l'inspection a echoué
     * @throws \Exception
     */
    public function fail();

    /**
     * Permet d'approuver ou désapprouver une inpection
     *
     * @return bool
     */
    public function inspect(\fakemock\app\Fake\FakeMethod $method_controller): bool;
}
