<?php

namespace fakemock\app\Inspector\Event;

class Twice extends \fakemock\app\Inspector\Event\CallEvent
{
    public function __construct()
    {
        parent::__construct(2);
    }
}
