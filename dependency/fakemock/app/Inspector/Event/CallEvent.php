<?php

namespace fakemock\app\Inspector\Event;

use InvalidArgumentException;

abstract class CallEvent
{
    /**
     * @var int $number_to_call_expected
     */
    protected $number_to_call_expected;

    public function __construct(int $number_to_call_expected = 0)
    {
        if ($number_to_call_expected > 0) {
            $this->number_to_call_expected = $number_to_call_expected;
        } else {
            throw new InvalidArgumentException("The number of calls must be greater than zero.");
        }
    }

    /**
     * Verifie le nombre d'appelle de la methode
     *
     * @param int $numberCalled Nombre de fois que la methode a été appelé
     * @return bool Vrai si la methode a été appelé le nombre de fois prévu par son bouchon
     */
    final public function verify(int $numberCalled): bool
    {
        return $this->number_to_call_expected === $numberCalled;
    }
}
