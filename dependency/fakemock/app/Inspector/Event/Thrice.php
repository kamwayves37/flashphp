<?php

namespace fakemock\app\Inspector\Event;

class Thrice extends \fakemock\app\Inspector\Event\CallEvent
{
    public function __construct()
    {
        parent::__construct(3);
    }
}
