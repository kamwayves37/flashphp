<?php

namespace fakemock\app\Inspector\Event;

class Once extends \fakemock\app\Inspector\Event\CallEvent
{
    public function __construct()
    {
        parent::__construct(1);
    }
}
