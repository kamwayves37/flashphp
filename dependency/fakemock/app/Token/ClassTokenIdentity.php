<?php

namespace fakemock\app\Token;

final class ClassTokenIdentity
{
    /**
     * @var array $identities
     */
    private $identities = [];

    /**
     * Permet de générer un jeton d'identification à partir d'un nom
     * 
     * @param string $name
     * @return void
     */
    public function register(string $key, $name): void
    {
        if (!$this->hasIdentity($key)) {
            $this->identities[$key] = $name;
        } else {
            throw new \Exception("A token in the name of {$name} already exists");
        }
    }

    /**
     * Permet de vérifier une identité à partir d'un nom
     * 
     * @param string $name
     * @return bool
     */
    public function hasIdentity(string $name): bool
    {
        return isset($this->identities[$name]);
    }

    /**
     * Permet d'obtenir un jeton correspondant a une clé;
     * 
     * @param string $key
     * @return string
     */
    public function getIdentityToken(string $key)
    {
        if ($this->hasIdentity($key)) {
            return $this->identities[$key];
        } else {
            throw new \Exception("No token corresponding to the name {$key}");
        }
    }
}
