<?php

namespace fakemock\app\Container;

use Closure;

final class BasicContainer
{
    /**
     * @var array $contents
     */
    private $contents = [];

    /**
     * @var array $instances
     */
    private $instances = [];

    /**
     * @var array
     */
    private $contents_security = [];

    /**
     * @param string $key
     * @param \Closure $action
     * @param bool $secure
     * @return void
     */
    public function set(string $key, \Closure $action, $secure = false)
    {
        if (!empty($key)) {

            if ($this->hasKey($key)) {
                if ($this->contents_security[$key]) {
                    throw new \Exception("Unable to modify a private action corresponding to the '{$key}' key.");
                }
            } else {
                $this->contents_security[$key] = $secure;
            }

            $this->contents[$key] = $action;
        } else {
            throw new \Exception("The specified key is incorrect, because it is empty.");
        }
    }

    /**
     * Récupere une instance correspondant a une clée
     * @param string $key
     * @return object
     * @throws \Exception
     */
    public function get(string $key, \Closure $default_action = null)
    {
        if (key_exists($key, $this->instances)) {
            return $this->instances[$key];
        } else {
            if ($this->hasKey($key)) {
                $object = $this->contents[$key]();
                if ($object !== null && is_object($object)) {
                    $this->instances[$key] = $object;
                    return $object;
                } else {
                    throw new \Exception("The initialization of the object in the container has failed, because the action associated with the '{$key}' key does not return an valid object.");
                }
            } else {
                if ($default_action !== null) {
                    $this->set($key, $default_action, true);
                    return $this->get($key);
                } else {
                    throw new \Exception("No action corresponding to the '{$key}' key in the container");
                }
            }
        }
    }

    /**
     * Obtenir l'action d'une clé de façon facultatif
     * Si la clé existe on peut soit rendre l'action de la clée
     * Ou modifier l'action de la clée.
     * Si la clée n'existe pas on peut soit l'enregistrer avec son action
     * Et le retourné ou non.
     * 
     * @param string $key
     * @return object
     * @throws \Exception
     */
    public function getOptional(string $key)
    {
        # code...if abcent
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasKey(string $key): bool
    {
        return key_exists($key, $this->contents);
    }
}
