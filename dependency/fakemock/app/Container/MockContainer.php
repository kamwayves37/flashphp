<?php

namespace fakemock\app\Container;

class MockContainer
{
    /**
     * @var BasicContainer $container_instance
     */
    private static $container_instance = null;

    /**
     *
     */
    private static function initDefaultDependency()
    {
        //Template class
        self::$container_instance->set("/fakemock/app/Generator/templates/class", function () {
            return new \fakemock\app\Generator\Template('./fakemock/app/Generator/templates/class.tpl');
        }, true);

        //Template method
        self::$container_instance->set("/fakemock/app/Generator/templates/method", function () {
            return new \fakemock\app\Generator\Template('./fakemock/app/Generator/templates/method.tpl');
        }, true);

        //Argument  any
        self::$container_instance->set(\fakemock\app\Arguments\Validation\Any::class, function () {
            return new \fakemock\app\Arguments\Validation\Any;
        }, true);

        //Argument  none
        self::$container_instance->set(\fakemock\app\Arguments\Validation\None::class, function () {
            return new \fakemock\app\Arguments\Validation\None;
        }, true);

        //Stub default value
        self::$container_instance->set(\fakemock\app\Types\Stub\StubDefaultValue::class, function () {
            return new \fakemock\app\Types\Stub\StubDefaultValue;
        }, true);

        //Inspector event once
        self::$container_instance->set(\fakemock\app\Inspector\Event\Once::class, function () {
            return new \fakemock\app\Inspector\Event\Once;
        }, true);

        //Inspector event twice
        self::$container_instance->set(\fakemock\app\Inspector\Event\Twice::class, function () {
            return new \fakemock\app\Inspector\Event\Twice;
        }, true);

        //Inspector event thrice
        self::$container_instance->set(\fakemock\app\Inspector\Event\Thrice::class, function () {
            return new \fakemock\app\Inspector\Event\Thrice;
        }, true);

        //Token identity
        self::$container_instance->set(\fakemock\app\Token\ClassTokenIdentity::class, function () {
            return new \fakemock\app\Token\ClassTokenIdentity;
        }, true);
    }

    /**
     *@return BasicContainer
     */
    public static function getContainer(): BasicContainer
    {
        if (self::$container_instance === null) {
            self::$container_instance = new BasicContainer();
            self::initDefaultDependency();
        }

        return self::$container_instance;
    }
}
