<?php

namespace fakemock\app\Fake;

/**
 * Fantôme
 * Permet de définir le comportement d'une methode ou d'une class fantome
 */
interface Fake
{
    /**
     * @param string $retun_type Determine le type de retour attendu lors de l'invoquation
     * @return object
     */
    public function invoke(array $args = [], string $retun_type = null);
}
