<?php

namespace fakemock\app\Fake;

use fakemock\util\Scanner;
use fakemock\app\Token\ClassTokenIdentity;
use fakemock\app\Container\MockContainer;

/**
 * Object  fantôme
 * Créer un object de class a partir d'une interface
 * Ou à partir d'une class
 */
final class FakeObject implements Fake
{
    /**
     * @var string $identity_name
     */
    private $identity_name;

    public function __construct()
    {
    }

    /**
     * Créer un Fake a partir d'une interface
     * @param $interface_namespace
     */
    public function implementWith($interface_name, string $new_namespace = "", string $class_name = "")
    {
        $scanner = new Scanner(new \ReflectionClass($interface_name));

        $this->identity_name = $scanner->getName();

        $traits = \fakemock\app\Doubler\Doubler::class;

        $namespace = empty($new_namespace) ? "fakemock" : $new_namespace;

        $name = empty($class_name) ?  $scanner->getShortName() : $class_name;

        MockContainer::getContainer()
            ->get(ClassTokenIdentity::class)
            ->register($this->identity_name, "\\{$namespace}\\{$name}");

        $class_evaluate = \fakemock\app\Generator\Generator::generateClass(
            [
                "namespace" => "{$namespace}",
                "name" => "{$name}",
                "interface_namespace" => "\\{$scanner->getName()}",
                "magic_traits" => "\\{$traits}",
                "methods" => "{$scanner->getMethods()}",
            ]
        );

        eval($class_evaluate);
    }

    /**
     * Créer un Fake a partir d'une class
     * @param $class_namespace
     */
    public function extendWith($class_namespace, string $new_namespace = "", string $class_name = "")
    {
        # code..
    }

    public function invoke(array $args = [], string $retun_type = null): object
    {
        try {

            $token_id_class_name = MockContainer::getContainer()
                ->get(ClassTokenIdentity::class)
                ->getIdentityToken($this->identity_name);

            return new $token_id_class_name;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
