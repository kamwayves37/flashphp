<?php

namespace fakemock\app\Fake;

use Exception;
use fakemock\app\Arguments\Arguments;
use fakemock\app\FakeMock;
use fakemock\app\Inspector\Inspector;
use InvalidArgumentException;

/**
 * Methode fantôme
 * Créer des methodes en implementant les methodes d'interface
 * Ou les methodes abstraite de class
 */
final class FakeMethod implements Fake
{
    /**
     * @var \Dummy $parent
     */
    private $parent;

    /**
     * @var \fakemock\app\types\StubType $stub
     */
    private $stub;

    /**
     * Type de retour de la fonction
     * @var string $return_type;
     */
    private $return_type;

    /**
     * Le nom de la méthode exploité
     * @var string $method_name;
     */
    private $method_name;

    /**
     * Type de retour de la fonction
     * @var int $number_of_calls;
     */
    private $number_of_calls;

    /**
     * @var \fakemock\app\Arguments\Arguments $argument
     */
    private $argument = null;

    /**
     * @var array $inspector
     */
    private $inspector = [];

    /**
     * @param object $parent
     * @param string $name
     * @param string $retun_type
     */
    public function __construct(object $parent, string $method_name = "", string $return_type = "")
    {
        $this->parent = $parent;
        $this->method_name = $method_name;
        $this->return_type = $return_type;
        $this->spy_was_called = false;
        $this->argument = \fakemock\app\Arguments\Args::any();
        $this->stub = FakeMock::triggerException(new \Exception("The method '{$this->method_name}' is defined as a dummy, You can't call it."));
        $this->number_of_calls = 0;
    }

    /**
     * Retourne Le nombre de fois que la méthode a été appelé
     * @return int
     */
    public function getNumberOfCalls(): int
    {
        return $this->number_of_calls;
    }

    /**
     *
     */
    public function getReturnType(): string
    {
        return $this->return_type;
    }

    /**
     *
     */
    public function getMethodeName(): string
    {
        return $this->method_name;
    }

    /**
     *
     */
    public function getStubReturnType(): string
    {
        return $this->stub->getValueType();
    }

    /**
     * @param \fakemock\app\Arguments\Arguments $args
     * @return \fakemock\app\Fake\FakeMethod
     */
    public function arguments(Arguments $args): FakeMethod
    {
        if ($args !== null) {
            $this->argument = $args;
        }

        return $this;
    }

    /**
     * @param \fakemock\app\Inspector\Inspector $inspector
     * @return \fakemock\app\Fake\FakeMethod
     */
    public function Inspect(Inspector $inspector): FakeMethod
    {
        if ($inspector !== null) {
            $class_name = get_class($inspector);
            if (!isset($this->inspector[$class_name]))
                $this->inspector[$class_name] = $inspector;
            else
                throw new \Exception("A '{$class_name}' type inspection rule has already been declared in the '{$this->method_name}' method controller.");
        }

        return $this;
    }

    /**
     * Determine la valeur stub à retourner lors de l'appelle de la méthode
     *
     * @param mixed $stub valeur fixe à retourner
     */
    public function willReturn($stub)
    {
        if (!($stub instanceof \fakemock\app\Types\StubType)) {
            if (is_callable($stub)) {
                $stub = FakeMock::onCallable($stub);
            } else {
                $stub = FakeMock::returnValue($stub);
            }
        }

        $stub->setMockContext($this->parent);
        $this->stub = $stub;
    }

    /**
     * Témoigne si la méthode a bien été appelé
     * @return bool
     */
    public function wasCalled(): bool
    {
        return $this->number_of_calls > 0;
    }

    /**
     * Retourner la valeur stub lorsque la méthode est appelé
     *
     * @param string $retun_type
     *@return any $stub value
     */
    public function invoke(array $args = [], string $return_type = null)
    {
        /**Initialisation du type de retour de la function */
        if ($this->return_type !== null) {
            $this->return_type = $return_type;
        } else {
            $this->return_type = "";
        }

        /**Validation des arguments */
        $this->argument->check($args);

        /**Configure l'objet stub */
        $this->stub->setArgument($args);
        $this->stub->setReturnType($return_type);

        /**Validation du type de retour */
        $stub_current_return = $this->getStubReturnType();
        if (!($stub_current_return === "NULL" && $return_type === "void")) {
            if (!empty($return_type) && $stub_current_return !== $return_type) {
                throw new \Exception("The stub value specified does not correspond to the expected return type\nexpected return: {$return_type}\ncurrent return: {$stub_current_return}\n");
            }
        }

        $value = $this->stub->getValue();

        $this->number_of_calls++;

        return $value;
    }

    /**
     *
     */
    public function investigate()
    {
        foreach ($this->inspector as $inspector) {
            if (!$inspector->inspect($this)) {
                $inspector->fail();
            }
        }
    }
}
