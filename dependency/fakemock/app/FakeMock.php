<?php

namespace fakemock\app;

use fakemock\app\Fake\FakeObject;
use fakemock\app\Types\Stub\StubValue;
use fakemock\app\Container\MockContainer;
use fakemock\app\Types\Stub\StubCallBack;
use fakemock\app\Types\Stub\StubException;
use fakemock\app\Types\Stub\StubArryIterator;
use fakemock\app\Types\Stub\StubThis;

/**
 * Fake Mock v1.0
 * Class de création de faux object virtuelle
 */
class FakeMock implements \fakemock\app\Types\Stub\Stub
{
    /**
     * @param string $name
     * @param string $namespace
     * @param string $class_name
     * @return \fakemock\app\Doubler\Doubler
     */
    final public static function create(string $name = "", string $namespace = "", string $class_name = "")
    {
        $class_identity = MockContainer::getContainer()
            ->get(\fakemock\app\Token\ClassTokenIdentity::class);

        if (!$class_identity->hasIdentity($name))
            self::generate($name, $namespace, $class_name);

        return MockContainer::getContainer()
            ->get($name)
            ->invoke();
    }

    /**
     * @param string $name
     * @param string $namespace
     * @param string $class_name
     * @return void
     */
    final public static function generate(string $name, string $namespace = "", string $class_name = ""): void
    {
        $class_identity = MockContainer::getContainer()
            ->get(\fakemock\app\Token\ClassTokenIdentity::class);

        if (!$class_identity->hasIdentity($name))
            MockContainer::getContainer()
                ->get($name, function () use ($name, $namespace, $class_name) {
                    $object = new FakeObject();

                    if (class_exists($name)) {
                        $object->extendWith($name);
                    } elseif (interface_exists($name)) {
                        $object->implementWith($name, $namespace, $class_name);
                    } else {
                        throw new \Exception("The namespace ''{$name} cannot be fake or moked");
                    }
                    return $object;
                });
        else
            throw new \Exception("A related mock with the namespace '{$name}' has already been created. Using the '{$class_identity->getIdentityToken($name)}' namespace to create an new instance");
    }

    /**
     * @param $namespace
     * @return MockBuilder
     */
    final public static function build($namespace)
    {
        return new MockBuilder($namespace);
    }

    /**
     * @param $value La valeur qui sera défini comme valeur stub
     * @return StubValue une intance de l'objet stub
     */
    final public static function returnValue($value): StubValue
    {
        return new StubValue($value);
    }

    /**
     * Retourne l'instance du mock ou la methode est appelé
     * 
     * @return StubThis
     */
    final public function returnThis(): StubThis
    {
        return new StubThis;
    }

    /**
     * @param \Exception $exception Lance une exception
     * @return \StubException La valeur stub
     */
    final public static function triggerException(\Exception $exception): StubException
    {
        return new StubException($exception);
    }

    /**
     * Retourne une valeur par défaut selon le type de retoun de la function
     */
    final public static function onDefaultValue()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Types\Stub\StubDefaultValue::class);
    }

    /**
     * Retourne une valeur de façon consécutif sur le tableau de valeurs
     *
     * @return any Une valeur de l'iteration
     */
    final public static function onEachCalls(...$values): StubArryIterator
    {
        return new StubArryIterator($values);
    }

    /**
     * @param \Closure $closure
     */
    final public static function onCallable($closure): StubCallBack
    {
        return new StubCallBack($closure);
    }

    /**
     * Permet de préciser a un bouchon que la methode doit s'appeler une seul fois
     * 
     * @return \fakemock\app\Inspector\Event\Once
     */
    final public static function once()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Inspector\Event\Once::class);
    }

    /**
     * Permet de préciser a un bouchon que la methode doit s'appeler deux fois
     */
    final public static function twice()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Inspector\Event\Twice::class);
    }

    /**
     * Permet de préciser a un bouchon que la methode doit s'appeler trois fois
     */
    final public static function thrice()
    {
        return MockContainer::getContainer()
            ->get(\fakemock\app\Inspector\Event\Thrice::class);
    }

    /**
     *
     */
    final public static function seconds(int $value)
    {
    }

    final public static function minutes(int $value)
    {
    }
}
