<?php

namespace autoloader\exception;

class AutoloaderException extends \Exception
{
    public function __construct($message, $code = 000)
    {
        parent::__construct($message, $code);
    }
}
