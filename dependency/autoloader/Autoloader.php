<?php

namespace autoloader;

use autoloader\exception\AutoloaderException;

/**
 * Autoloader v1.0
 *
 * Class de chargement automatique des classes
 */
class Autoloader
{
    public static function autoload(string $class_name)
    {
        //Ajoute au nom de la class, la root principale de dependence et l'extension du fichier (.php)
        $classFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class_name) . '.php';

        //Vérifie que le fichier existe
        if (file_exists($classFile)) {

            //Includ le fichier s'il n'est pas encore inclus
            include_once $classFile;

            //Vérifier que le fichier contient une classe et que la convension de la POO est respecté (Le nom de fichier doit être le même nom que celui la classe)
            if (!class_exists($class_name) &&
                !interface_exists($class_name) &&
                !trait_exists($class_name)) {
                //Exception si le fichier ne contient pas de class définir
                throw new AutoloaderException("$class_name  is not a class or does not define a class of that name.", 002);
            }

        } else {
            //Exception si le fichier n'existe pas
            throw new AutoloaderException("The file $class_name  is untraceable or non-existent to $classFile", 001);
        }
    }
}

// Definir l'extension de fichier
spl_autoload_extensions('.php');

//Enregistre l'autoloader
spl_autoload_register(array('autoloader\Autoloader', 'autoload'));
